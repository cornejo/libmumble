#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/select.h>
#include <sys/time.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <netinet/ip.h>

#include <talloc.h>
#include <openssl/ssl.h>
#include <openssl/err.h>

#include "ssl_connection.h"
#include "mumble.pb-c.h"
#include "libmumble.h"

#define CLIENT_VERSION 0x010203
#define CLIENT_OS "OS Unknown"
#define CLIENT_RELEASE "OS Release Unknown"
#define CLIENT_OS_VERSION "1"

#define WARN_UNUSED __attribute__((warn_unused_result))

enum MumbleClientState
{
  MUMBLE_CLIENTSTATE_INPROGRESS = 0,
  MUMBLE_CLIENTSTATE_CONNECTED,
  MUMBLE_CLIENTSTATE_FAILED
};

struct mumble_struct
{
  struct ssl_connection_struct* ssl_connection;
  enum MumbleClientState state;
  char* host;
  char* port;
  char* server_password;
  char* username;
  time_t last_ping;

  mumble_version_callback_t*             version_callback;
  mumble_audio_callback_t*               audio_callback;
  mumble_serversync_callback_t*          serversync_callback;
  mumble_channelremove_callback_t*       channelremove_callback;
  mumble_channelstate_callback_t*        channelstate_callback;
  mumble_userremove_callback_t*          userremove_callback;
  mumble_userstate_callback_t*           userstate_callback;
  mumble_banlist_callback_t*             banlist_callback;
  mumble_textmessage_callback_t*         textmessage_callback;
  mumble_permissiondenied_callback_t*    permissiondenied_callback;
  mumble_acl_callback_t*                 acl_callback;
  mumble_queryusers_callback_t*          queryusers_callback;
  mumble_cryptsetup_callback_t*          cryptsetup_callback;
  mumble_contextactionmodify_callback_t* contextactionmodify_callback;
  mumble_contextaction_callback_t*       contextaction_callback;
  mumble_userlist_callback_t*            userlist_callback;
  mumble_voicetarget_callback_t*         voicetarget_callback;
  mumble_permissionquery_callback_t*     permissionquery_callback;
  mumble_codecversion_callback_t*        codecversion_callback;
  mumble_userstats_callback_t*           userstats_callback;
  mumble_requestblob_callback_t*         requestblob_callback;
  mumble_serverconfig_callback_t*        serverconfig_callback;
  mumble_suggestconfig_callback_t*       suggestconfig_callback;

  void* user_data;
};

enum MumbleMessageTypes
{
  MUMBLE_MESSAGE_VERSION             =  0,
  MUMBLE_MESSAGE_UDPTUNNEL           =  1,
  MUMBLE_MESSAGE_AUTHENTICATE        =  2,
  MUMBLE_MESSAGE_PING                =  3,
  MUMBLE_MESSAGE_REJECT              =  4,
  MUMBLE_MESSAGE_SERVERSYNC          =  5,
  MUMBLE_MESSAGE_CHANNELREMOVE       =  6,
  MUMBLE_MESSAGE_CHANNELSTATE        =  7,
  MUMBLE_MESSAGE_USERREMOVE          =  8,
  MUMBLE_MESSAGE_USERSTATE           =  9,
  MUMBLE_MESSAGE_BANLIST             = 10,
  MUMBLE_MESSAGE_TEXTMESSAGE         = 11,
  MUMBLE_MESSAGE_PERMISSIONDENIED    = 12,
  MUMBLE_MESSAGE_ACL                 = 13,
  MUMBLE_MESSAGE_QUERYUSERS          = 14,
  MUMBLE_MESSAGE_CRYPTSETUP          = 15,
  MUMBLE_MESSAGE_CONTEXTACTIONMODIFY = 16,
  MUMBLE_MESSAGE_CONTEXTACTION       = 17,
  MUMBLE_MESSAGE_USERLIST            = 18,
  MUMBLE_MESSAGE_VOICETARGET         = 19,
  MUMBLE_MESSAGE_PERMISSIONQUERY     = 20,
  MUMBLE_MESSAGE_CODECVERSION        = 21,
  MUMBLE_MESSAGE_USERSTATS           = 22,
  MUMBLE_MESSAGE_REQUESTBLOB         = 23,
  MUMBLE_MESSAGE_SERVERCONFIG        = 24,
  MUMBLE_MESSAGE_SUGGESTCONFIG       = 25 
};

// ****************** PRIVATE FUNCTIONS **************************

// Message 0
static int mumble_private_process_version(struct mumble_struct* context, uint8_t* message, uint32_t message_size)
{
  if (context->version_callback != NULL)
  {
    MumbleProto__Version* version = mumble_proto__version__unpack(NULL, message_size, message);

    context->version_callback(version->version >> 16, version->version >> 8 & 0xff, version->version & 0xff, version->release, version->os, version->os_version, context->user_data);

    mumble_proto__version__free_unpacked(version, NULL);
  }

  return 0;
}

// Message 1
static int mumble_private_process_udptunnel(struct mumble_struct* context, uint8_t* message, uint32_t message_size)
{
  // Not a protobuf struct.
  // Refer to PDF for audio structure
  if (context->audio_callback != NULL)
  {
    context->audio_callback(message, message_size, context->user_data);
  }

  return 0;
}

// Message 2 (Authenticate) should never be received by the client
static int mumble_private_process_authenticate(struct mumble_struct* context, uint8_t* message, uint32_t message_size)
{
  printf("WARNING: Authenticate message received after authenticated?\n");

  return 0;
}

// Message 3
static int mumble_private_process_ping(struct mumble_struct* context, uint8_t* message, uint32_t message_size)
{
  MumbleProto__Ping* ping  = mumble_proto__ping__unpack(NULL, message_size, message);
  context->last_ping = time(NULL);

  printf("Pong Good/Late/Lost ");
  if (ping->has_good)
  {
    printf("%d/", ping->good);
  } else
  {
    printf("?/");
  }
  if (ping->has_late)
  {
    printf("%d/", ping->late);
  } else
  {
    printf("?/");
  }
  if (ping->has_lost)
  {
    printf("%d", ping->lost);
  } else
  {
    printf("?");
  }

  if (ping->has_udp_ping_avg)
  {
    printf("Ping: %0.2fms\n", ping->udp_ping_avg);
  }

  printf("\n");

  mumble_proto__ping__free_unpacked(ping, NULL);

  return 0;
}

// Message 4
static int mumble_private_process_reject(struct mumble_struct* context, uint8_t* message, uint32_t message_size)
{
  MumbleProto__Reject* reject = mumble_proto__reject__unpack(NULL, message_size, message);

  printf("Failed to authenticate! Reason: ");

  if (reject->has_type)
  {
    switch (reject->type)
    {
    case MUMBLE_PROTO__REJECT__REJECT_TYPE__None:
      printf("No reason provided");
      break;
    case MUMBLE_PROTO__REJECT__REJECT_TYPE__WrongVersion:
      printf("Wrong Version");
      break;
    case MUMBLE_PROTO__REJECT__REJECT_TYPE__InvalidUsername:
      printf("Invalid Username");
      break;
    case MUMBLE_PROTO__REJECT__REJECT_TYPE__WrongUserPW:
      printf("Wrong User Password");
      break;
    case MUMBLE_PROTO__REJECT__REJECT_TYPE__WrongServerPW:
      printf("Wrong Server Password");
      break;
    case MUMBLE_PROTO__REJECT__REJECT_TYPE__UsernameInUse:
      printf("Username In Use");
      break;
    case MUMBLE_PROTO__REJECT__REJECT_TYPE__ServerFull:
      printf("Server Full");
      break;
    case MUMBLE_PROTO__REJECT__REJECT_TYPE__NoCertificate:
      printf("No Certificate Provided");
      break;
    case MUMBLE_PROTO__REJECT__REJECT_TYPE__AuthenticatorFail:
      printf("Authenticator Fail");
      break;
    default:
      printf("Invalid type received!");
    }
  }
  
  if (reject->reason)
  {
    printf(" (%s)", reject->reason);
  }
  printf("\n");

  mumble_proto__reject__free_unpacked(reject, NULL);

  context->state = MUMBLE_CLIENTSTATE_FAILED;

  return 0;
}

// Message 5
static int mumble_private_process_serversync(struct mumble_struct* context, uint8_t* message, uint32_t message_size)
{
  if (context->serversync_callback != NULL)
  {
    MumbleProto__ServerSync* server_sync = mumble_proto__server_sync__unpack(NULL, message_size, message);

    int32_t session = server_sync->has_session ? server_sync->session : -1;
    int32_t max_bandwidth = server_sync->has_max_bandwidth ? server_sync->max_bandwidth : -1;
    int64_t permissions = server_sync->has_permissions ? server_sync->permissions : -1;
    context->serversync_callback(server_sync->welcome_text, session, max_bandwidth, permissions, context->user_data);

    mumble_proto__server_sync__free_unpacked(server_sync, NULL);
  }

  context->state = MUMBLE_CLIENTSTATE_CONNECTED;

  return 0;
}

// Message 6
static int mumble_private_process_channelremove(struct mumble_struct* context, uint8_t* message, uint32_t message_size)
{
  if (context->channelremove_callback != NULL)
  {
    MumbleProto__ChannelRemove* channel_remove = mumble_proto__channel_remove__unpack(NULL, message_size, message);

    context->channelremove_callback(channel_remove->channel_id, context->user_data);

    mumble_proto__channel_remove__free_unpacked(channel_remove, NULL);
  }

  return 0;
}

// Message 7
static int mumble_private_process_channelstate(struct mumble_struct* context, uint8_t* message, uint32_t message_size)
{
  if (context->channelstate_callback != NULL)
  {
    MumbleProto__ChannelState* channelstate = mumble_proto__channel_state__unpack(NULL, message_size, message);

    char* name = channelstate->name;
    int32_t channel_id = channelstate->has_channel_id ? channelstate->channel_id : -1;
    int32_t parent = channelstate->has_parent ? channelstate->parent : -1;
    char* description = channelstate->description;
    uint32_t* links = channelstate->links;
    uint32_t n_links = channelstate->n_links;
    uint32_t* links_add = channelstate->links_add;
    uint32_t n_links_add = channelstate->n_links_add;
    uint32_t* links_remove = channelstate->links_remove;
    uint32_t n_links_remove = channelstate->n_links_remove;
    int32_t temporary = channelstate->has_temporary ? channelstate->temporary : 0; // Default is false
    int32_t position = channelstate->has_position ? channelstate->position : 0; // Default is 0
    // Skip description hash

    context->channelstate_callback(name, channel_id, parent, description, links, n_links, links_add, n_links_add, links_remove, n_links_remove, temporary, position, context->user_data);

    mumble_proto__channel_state__free_unpacked(channelstate, NULL);
  }

  return 0;
}

// Message 8
static int mumble_private_process_userremove(struct mumble_struct* context, uint8_t* message, uint32_t message_size)
{
  if (context->userremove_callback != NULL)
  {
    MumbleProto__UserRemove* user_remove = mumble_proto__user_remove__unpack(NULL, message_size, message);

    int32_t actor = user_remove->has_actor ? user_remove->actor : -1;
    int32_t ban = user_remove->has_ban ? user_remove->ban : -1;

    context->userremove_callback(user_remove->session, actor, user_remove->reason, ban, context->user_data);

    mumble_proto__user_remove__free_unpacked(user_remove, NULL);
  }

  return 0;
}

// Message 9
static int mumble_private_process_userstate(struct mumble_struct* context, uint8_t* message, uint32_t message_size)
{
  if (context->userstate_callback != NULL)
  {
    MumbleProto__UserState* user_state = mumble_proto__user_state__unpack(NULL, message_size, message);
 
    // There are far too many things in this structure. Culling to the ones that are probably important
    int32_t session = user_state->has_session ? user_state->session : -1;
    int32_t actor = user_state->has_actor ? user_state->actor : -1;
    char* name = user_state->name;
    int32_t user_id = user_state->has_user_id ? user_state->user_id : -1;
    int32_t channel_id = user_state->has_channel_id ? user_state->channel_id : -1;
    int32_t mute = user_state->has_mute ? user_state->mute : -1;
    int32_t deaf = user_state->has_deaf ? user_state->deaf : -1;
    int32_t suppress = user_state->has_suppress ? user_state->suppress : -1;
    int32_t self_mute = user_state->has_self_mute ? user_state->self_mute : -1;
    int32_t self_deaf = user_state->has_self_deaf ? user_state->self_deaf : -1;
    // Skip texture
    // Skip plugin context
    // Skip plugin identity
    char* comment = user_state->comment;
    // Skip hash
    // Skip comment hash
    // Skip texture hash
    int32_t priority_speaker = user_state->has_priority_speaker ? user_state->priority_speaker : -1;
    int32_t recording = user_state->has_recording ? user_state->recording : -1;

    context->userstate_callback(session, actor, name, user_id, channel_id, mute, deaf, suppress, self_mute, self_deaf, comment, priority_speaker, recording, context->user_data);
  
    mumble_proto__user_state__free_unpacked(user_state, NULL);
  }

  return 0;
}

// Message 10
static int mumble_private_process_banlist(struct mumble_struct* context, uint8_t* message, uint32_t message_size)
{
  if (context->banlist_callback != NULL)
  {
    MumbleProto__BanList* ban_list = mumble_proto__ban_list__unpack(NULL, message_size, message);

    for (int i = 0; i < ban_list->n_bans; i++)
    {
      MumbleProto__BanList__BanEntry* ban = ban_list->bans[i];

      uint8_t* ip_data = ban->address.data;
      uint32_t ip_data_size = ban->address.len;
      uint32_t mask = ban->mask;
      char* name = ban->name;
      char* hash = ban->hash;
      char* reason = ban->reason;
      char* start = ban->start;
      int32_t duration = ban->has_duration ? ban->duration : -1;

      context->banlist_callback(ip_data, ip_data_size, mask, name, hash, reason, start, duration, context->user_data);
    }

    mumble_proto__ban_list__free_unpacked(ban_list, NULL);
  }
  return 0;
}

// Message 11
static int mumble_private_process_textmessage(struct mumble_struct* context, uint8_t* message, uint32_t message_size)
{
  if (context->textmessage_callback != NULL)
  {
    MumbleProto__TextMessage* text_message = mumble_proto__text_message__unpack(NULL, message_size, message);
  
    int32_t actor = text_message->has_actor ? text_message->actor : -1;
    uint32_t n_session = text_message->n_session;
    uint32_t* session = text_message->session;
    uint32_t n_channel_id = text_message->n_channel_id;
    uint32_t* channel_id = text_message->channel_id;
    uint32_t n_tree_id = text_message->n_tree_id;
    uint32_t* tree_id = text_message->tree_id;
    char* message = text_message->message;

    context->textmessage_callback(actor, n_session, session, n_channel_id, channel_id, n_tree_id, tree_id, message, context->user_data);

    mumble_proto__text_message__free_unpacked(text_message, NULL);
  }

  return 0;
}

// Message 12
static int mumble_private_process_permissiondenied(struct mumble_struct* context, uint8_t* message, uint32_t message_size)
{
  if (context->permissiondenied_callback != NULL)
  {
    MumbleProto__PermissionDenied* permission_denied = mumble_proto__permission_denied__unpack(NULL, message_size, message);

    int32_t permission = permission_denied->has_permission ? permission_denied->permission : -1;
    int32_t channel_id = permission_denied->has_channel_id ? permission_denied->channel_id : -1;
    int32_t session = permission_denied->has_session ? permission_denied->session : -1;
    char* reason = permission_denied->reason;
    int32_t deny_type = permission_denied->has_type ? permission_denied->type : -1;
    char* name = permission_denied->name;

    context->permissiondenied_callback(permission, channel_id, session, reason, deny_type, name, context->user_data);

    mumble_proto__permission_denied__free_unpacked(permission_denied, NULL);
  }

  return 0;
}

// Message 13
static int mumble_private_process_acl(struct mumble_struct* context, uint8_t* message, uint32_t message_size)
{
  if (context->acl_callback != NULL)
  {
    // Changroup
//  ProtobufCMessage base;
//  char *name;
//  protobuf_c_boolean has_inherited;
//  protobuf_c_boolean inherited;
//  protobuf_c_boolean has_inherit;
//  protobuf_c_boolean inherit;
//  protobuf_c_boolean has_inheritable;
//  protobuf_c_boolean inheritable;
//  size_t n_add;
//  uint32_t *add;
//  size_t n_remove;
//  uint32_t *remove;
//  size_t n_inherited_members;
//  uint32_t *inherited_members;
//
//  Chan ACL
//  ProtobufCMessage base;
//  protobuf_c_boolean has_apply_here;
//  protobuf_c_boolean apply_here;
//  protobuf_c_boolean has_apply_subs;
//  protobuf_c_boolean apply_subs;
//  protobuf_c_boolean has_inherited;
//  protobuf_c_boolean inherited;
//  protobuf_c_boolean has_user_id;
//  uint32_t user_id;
//  char *group;
//  protobuf_c_boolean has_grant;
//  uint32_t grant;
//  protobuf_c_boolean has_deny;
//  uint32_t deny;
//
//  ACL
//  ProtobufCMessage base;
//  uint32_t channel_id;
//  protobuf_c_boolean has_inherit_acls;
//  protobuf_c_boolean inherit_acls;
//  size_t n_groups;
//  MumbleProto__ACL__ChanGroup **groups;
//  size_t n_acls;
//  MumbleProto__ACL__ChanACL **acls;
//  protobuf_c_boolean has_query;
//  protobuf_c_boolean query;
//
    MumbleProto__ACL* acl = mumble_proto__acl__unpack(NULL, message_size, message);

    printf("ACL message not currently supported\n");

    mumble_proto__acl__free_unpacked(acl, NULL);
  }

  return 0;
}

// Message 14
static int mumble_private_process_queryusers(struct mumble_struct* context, uint8_t* message, uint32_t message_size)
{
  if (context->queryusers_callback != NULL)
  {
    MumbleProto__QueryUsers* query_users = mumble_proto__query_users__unpack(NULL, message_size, message);

    uint32_t n_ids = query_users->n_ids;
    uint32_t* ids = query_users->ids;
    uint32_t n_names = query_users->n_names;
    char** names = query_users->names;

    context->queryusers_callback(n_ids, ids, n_names, names, context->user_data);

    mumble_proto__query_users__free_unpacked(query_users, NULL);
  }

  return 0;
}

// Message 15
static int mumble_private_process_cryptsetup(struct mumble_struct* context, uint8_t* message, uint32_t message_size)
{
  if (context->cryptsetup_callback != NULL)
  {
    MumbleProto__CryptSetup* cryptsetup = mumble_proto__crypt_setup__unpack(NULL, message_size, message);

    uint32_t key_size = 0;
    uint8_t* key = NULL;
    uint32_t client_nonce_size = 0;
    uint8_t* client_nonce = NULL;
    uint32_t server_nonce_size = 0;
    uint8_t* server_nonce = NULL;

    if (cryptsetup->has_key)
    {
      key_size = cryptsetup->key.len;
      key = cryptsetup->key.data;
    }

    if (cryptsetup->has_client_nonce)
    {
      client_nonce_size = cryptsetup->client_nonce.len;
      client_nonce = cryptsetup->client_nonce.data;
    }

    if (cryptsetup->has_server_nonce)
    {
      server_nonce_size = cryptsetup->server_nonce.len;
      server_nonce = cryptsetup->server_nonce.data;
    }
  
    context->cryptsetup_callback(key_size, key, client_nonce_size, client_nonce, server_nonce_size, server_nonce, context->user_data);

    mumble_proto__crypt_setup__free_unpacked(cryptsetup, NULL);
  }

  return 0;
}

// Message 16
static int mumble_private_process_contextactionmodify(struct mumble_struct* context, uint8_t* message, uint32_t message_size)
{
  if (context->contextactionmodify_callback != NULL)
  {
    MumbleProto__ContextActionModify* context_action_modify = mumble_proto__context_action_modify__unpack(NULL, message_size, message);

    char* action = context_action_modify->action;
    char* text = context_action_modify->text;
    uint32_t m_context = context_action_modify->has_context ? context_action_modify->context : 0; // Bit flags
    uint32_t operation = context_action_modify->has_operation ? context_action_modify->operation : 0; // Add/remove

    context->contextactionmodify_callback(action, text, m_context, operation, context->user_data);

    mumble_proto__context_action_modify__free_unpacked(context_action_modify, NULL);
  }

  return 0;
}

// Message 17
static int mumble_private_process_contextaction(struct mumble_struct* context, uint8_t* message, uint32_t message_size)
{
  if (context->contextaction_callback != NULL)
  {
    MumbleProto__ContextAction* context_action = mumble_proto__context_action__unpack(NULL, message_size, message);

    int32_t session = context_action->has_session ? context_action->session : -1;
    int32_t channel_id = context_action->has_channel_id ? context_action->channel_id : -1;
    char* action = context_action->action;

    context->contextaction_callback(session, channel_id, action, context->user_data);

    mumble_proto__context_action__free_unpacked(context_action, NULL);
  }

  return 0;
}

// Message 18
static int mumble_private_process_userlist(struct mumble_struct* context, uint8_t* message, uint32_t message_size)
{
  if (context->userlist_callback != NULL)
  {
    MumbleProto__UserList* user_list = mumble_proto__user_list__unpack(NULL, message_size, message);

    for (int i = 0; i < user_list->n_users; i++)
    {
      MumbleProto__UserList__User* user = user_list->users[i];

      uint32_t user_id = user->user_id;
      char* name = user->name;
      char* last_seen = user->last_seen;
      int32_t last_channel = user->has_last_channel ? user->last_channel : -1;

      context->userlist_callback(user_id, name, last_seen, last_channel, context->user_data);
    }

    mumble_proto__user_list__free_unpacked(user_list, NULL);
  }

  return 0;
}

// Message 19
static int mumble_private_process_voicetarget(struct mumble_struct* context, uint8_t* message, uint32_t message_size)
{
  if (context->voicetarget_callback != NULL)
  {
    MumbleProto__VoiceTarget* voice_target = mumble_proto__voice_target__unpack(NULL, message_size, message);

    printf("VoiceTarget Message: I don't think the server ever sends this structure....\n");

    mumble_proto__voice_target__free_unpacked(voice_target, NULL);
  }

  return 0;
}

// Message 20
static int mumble_private_process_permission_query(struct mumble_struct* context, uint8_t* message, uint32_t message_size)
{
  if (context->permissionquery_callback != NULL)
  {
    MumbleProto__PermissionQuery* permission_query = mumble_proto__permission_query__unpack(NULL, message_size, message);

    int32_t channel_id = permission_query->has_channel_id ? permission_query->channel_id : -1;
    uint32_t permissions = permission_query->has_permissions ? permission_query->permissions : 0;
    uint32_t flush = permission_query->has_flush ? permission_query->flush : -1;

    context->permissionquery_callback(channel_id, permissions, flush, context->user_data);

    mumble_proto__permission_query__free_unpacked(permission_query, NULL);
  }

  return 0;
}

// Message 21
static int mumble_private_process_codecversion(struct mumble_struct* context, uint8_t* message, uint32_t message_size)
{
  if (context->codecversion_callback != NULL)
  {
    MumbleProto__CodecVersion* codec_version = mumble_proto__codec_version__unpack(NULL, message_size, message);

    int32_t alpha = codec_version->alpha;
    int32_t beta = codec_version->beta;
    uint32_t prefer_alpha = codec_version->prefer_alpha;
    int32_t opus = codec_version->has_opus ? codec_version->opus : 0;

    context->codecversion_callback(alpha, beta, prefer_alpha, opus, context->user_data);

    mumble_proto__codec_version__free_unpacked(codec_version, NULL);
  }

  return 0;
}

// Message 22
static int mumble_private_process_userstats(struct mumble_struct* context, uint8_t* message, uint32_t message_size)
{
  if (context->userstats_callback != NULL)
  {
    // Userstats_stats
//  ProtobufCMessage base;
//  protobuf_c_boolean has_good;
//  uint32_t good;
//  protobuf_c_boolean has_late;
//  uint32_t late;
//  protobuf_c_boolean has_lost;
//  uint32_t lost;
//  protobuf_c_boolean has_resync;
//  uint32_t resync;
//
//  userstats
//
//  ProtobufCMessage base;
//  protobuf_c_boolean has_session;
//  uint32_t session;
//  protobuf_c_boolean has_stats_only;
//  protobuf_c_boolean stats_only;
//  size_t n_certificates;
//  ProtobufCBinaryData *certificates;
//  MumbleProto__UserStats__Stats *from_client;
//  MumbleProto__UserStats__Stats *from_server;
//  protobuf_c_boolean has_udp_packets;
//  uint32_t udp_packets;
//  protobuf_c_boolean has_tcp_packets;
//  uint32_t tcp_packets;
//  protobuf_c_boolean has_udp_ping_avg;
//  float udp_ping_avg;
//  protobuf_c_boolean has_udp_ping_var;
//  float udp_ping_var;
//  protobuf_c_boolean has_tcp_ping_avg;
//  float tcp_ping_avg;
//  protobuf_c_boolean has_tcp_ping_var;
//  float tcp_ping_var;
//  MumbleProto__Version *version;
//  size_t n_celt_versions;
//  int32_t *celt_versions;
//  protobuf_c_boolean has_address;
//  ProtobufCBinaryData address;
//  protobuf_c_boolean has_bandwidth;
//  uint32_t bandwidth;
//  protobuf_c_boolean has_onlinesecs;
//  uint32_t onlinesecs;
//  protobuf_c_boolean has_idlesecs;
//  uint32_t idlesecs;
//  protobuf_c_boolean has_strong_certificate;
//  protobuf_c_boolean strong_certificate;
//  protobuf_c_boolean has_opus;
//  protobuf_c_boolean opus;
    MumbleProto__UserStats* user_stats = mumble_proto__user_stats__unpack(NULL, message_size, message);

    printf("UserStats message not implemented yet\n");

    mumble_proto__user_stats__free_unpacked(user_stats, NULL);
  }

  return 0;
}

// Message 23
static int mumble_private_process_requestblob(struct mumble_struct* context, uint8_t* message, uint32_t message_size)
{
  if (context->requestblob_callback != NULL)
  {
    MumbleProto__RequestBlob* request_blob = mumble_proto__request_blob__unpack(NULL, message_size, message);

    printf("RequestBlob Message: I don't think this is sent by the server\n");

    mumble_proto__request_blob__free_unpacked(request_blob, NULL);
  }

  return 0;
}

// Message 24
static int mumble_private_process_serverconfig(struct mumble_struct* context, uint8_t* message, uint32_t message_size)
{
  if (context->serverconfig_callback != NULL)
  {
    MumbleProto__ServerConfig* server_config = mumble_proto__server_config__unpack(NULL, message_size, message);
  
    uint32_t max_bandwidth = server_config->has_max_bandwidth ? server_config->max_bandwidth : 0;
    char* welcome_text = server_config->welcome_text;
    uint32_t allow_html = server_config->has_allow_html ? server_config->allow_html : 0;
    uint32_t message_length = server_config->has_message_length ? server_config->message_length : 0;
    uint32_t image_message_length = server_config->has_image_message_length ? server_config->image_message_length : 0;

    context->serverconfig_callback(max_bandwidth, welcome_text, allow_html, message_length, image_message_length, context->user_data);

    mumble_proto__server_config__free_unpacked(server_config, NULL);
  }

  return 0;
}

// Message 25
static int mumble_private_process_suggestconfig(struct mumble_struct* context, uint8_t* message, uint32_t message_size)
{
  if (context->suggestconfig_callback != NULL)
  {
    MumbleProto__SuggestConfig* suggest_config = mumble_proto__suggest_config__unpack(NULL, message_size, message);

    uint32_t version = suggest_config->has_version ? suggest_config->version : 0;
    uint32_t positional = suggest_config->has_positional ? suggest_config->positional : 0;
    uint32_t push_to_talk = suggest_config->has_push_to_talk ? suggest_config->push_to_talk : 0;

    context->suggestconfig_callback(version, positional, push_to_talk, context->user_data);

    mumble_proto__suggest_config__free_unpacked(suggest_config, NULL);
  }

  return 0;
}

// **********************************************
// End of messages
// **********************************************

// type is out
// buffer in in/out
// size is in
static int WARN_UNUSED mumble_private_read(struct mumble_struct* context, uint16_t* type, uint8_t* buffer, uint32_t size, struct timeval* tv, fd_set* readfds, fd_set* writefds, fd_set *exceptfds, int max_fd)
{
  int ret = ssl_connection_wait_for_data_available(context->ssl_connection, tv, readfds, writefds, exceptfds, max_fd);
  
  if (ret <= 0)
  {
    return ret;
  }

  *type = 0;
  ret = ssl_connection_read(context->ssl_connection, type, sizeof(uint16_t));
  if (ret != sizeof(uint16_t))
  {
    printf("Failed to read message type\n");
    return -1;
  }
  // Network byte order. Need to fix
  *type = ntohs(*type);

  uint32_t length = 0;
  ret = ssl_connection_read(context->ssl_connection, &length, sizeof(length));
  if (ret != sizeof(uint32_t))
  {
    printf("Failed to read length of message\n");
    return -1;
  }
  // Network byte order. Need to fix
  length = ntohl(length);

  if (ret > size)
  {
    printf("rx buffer not large enough\n");
    // Gotta drain it anyway
    while (ret > 0)
    {
      int amount = ret > size ? size : ret;
      ret -= ssl_connection_read(context->ssl_connection, buffer, amount);
    }
    return -1;
  }

  if (length > 0)
  {
    ret = ssl_connection_read(context->ssl_connection, buffer, length);
    if (ret != length)
    {
      printf("Failed to read complete message. I wanted %d bytes, read %d bytes\n", length, ret);
      return -1;
    }
  }

  return length;
}

static int WARN_UNUSED mumble_private_write(struct mumble_struct* context, uint16_t type, uint8_t* buffer, uint32_t size)
{
  // Change its to network byte order
  uint16_t type_network = ntohs(type);
  uint32_t size_network = ntohl(size);
  
  uint32_t large_buffer_size = sizeof(type) + sizeof(size) + size;
  uint8_t* large_buffer = (uint8_t*)talloc_size(context, large_buffer_size);
  memcpy(large_buffer, &type_network, sizeof(type));
  memcpy(large_buffer + sizeof(type), &size_network, sizeof(size));
  memcpy(large_buffer + sizeof(type) + sizeof(size), buffer, size);

  int ret = ssl_connection_write(context->ssl_connection, large_buffer, large_buffer_size);
  talloc_free(large_buffer);

  if (ret != large_buffer_size)
  {
    // Failed to send the entire amount... damn
    return 0;
  }
  return size;
}


static int mumble_private_send_version(struct mumble_struct* context)
{
  MumbleProto__Version version = MUMBLE_PROTO__VERSION__INIT;
  version.version = CLIENT_VERSION;
  version.has_version = 1;
  version.os = CLIENT_OS;
  version.release = CLIENT_RELEASE;
  version.os_version = CLIENT_OS_VERSION;

  uint32_t message_size = mumble_proto__version__get_packed_size(&version);

  uint8_t* message = (uint8_t*)talloc_size(context, message_size);
  mumble_proto__version__pack(&version, message);

  int ret = mumble_private_write(context, MUMBLE_MESSAGE_VERSION, message, message_size);

  talloc_free(message);

  if (ret != message_size)
  {
    printf("Failed to send complete version message\n");
    return -1;
  }

  return 0;
}

static int mumble_private_authenticate(struct mumble_struct* context)
{
  MumbleProto__Authenticate authenticate = MUMBLE_PROTO__AUTHENTICATE__INIT;
  authenticate.username = context->username;
  authenticate.password = context->server_password;

  authenticate.n_tokens = 0;
  authenticate.tokens = NULL;

  authenticate.n_celt_versions = 0;
  authenticate.celt_versions = NULL;

  authenticate.has_opus = 1;
  authenticate.opus = 1;

  uint32_t message_size = mumble_proto__authenticate__get_packed_size(&authenticate);

  uint8_t* message = (uint8_t*)talloc_size(context, message_size);
  mumble_proto__authenticate__pack(&authenticate, message);

  int ret = mumble_private_write(context, MUMBLE_MESSAGE_AUTHENTICATE, message, message_size);
  talloc_free(message);

  if (ret != message_size)
  {
    printf("Failed to send complete authentication message\n");
    return -1;
  }

  return 0;
}

static int mumble_private_process_message(struct mumble_struct* context, uint16_t message_type, uint8_t* message, uint32_t message_size)
{
  switch (message_type)
  {
  case MUMBLE_MESSAGE_VERSION: // 0
    return mumble_private_process_version(context, message, message_size);

  case MUMBLE_MESSAGE_UDPTUNNEL: // 1
    return mumble_private_process_udptunnel(context, message, message_size);

  case MUMBLE_MESSAGE_AUTHENTICATE: // 2
    return mumble_private_process_authenticate(context, message, message_size);

  case MUMBLE_MESSAGE_PING: // 3
    return mumble_private_process_ping(context, message, message_size);

  case MUMBLE_MESSAGE_REJECT: // 4
    return mumble_private_process_reject(context, message, message_size);

  case MUMBLE_MESSAGE_SERVERSYNC: // 5
    return mumble_private_process_serversync(context, message, message_size);

  case MUMBLE_MESSAGE_CHANNELREMOVE: // 6
    return mumble_private_process_channelremove(context, message, message_size);

  case MUMBLE_MESSAGE_CHANNELSTATE: // 7
    return mumble_private_process_channelstate(context, message, message_size);

  case MUMBLE_MESSAGE_USERREMOVE: // 8
    return mumble_private_process_userremove(context, message, message_size);

  case MUMBLE_MESSAGE_USERSTATE: // 9
    return mumble_private_process_userstate(context, message, message_size);

  case MUMBLE_MESSAGE_BANLIST: // 10
    return mumble_private_process_banlist(context, message, message_size);

  case MUMBLE_MESSAGE_TEXTMESSAGE: // 11
    return mumble_private_process_textmessage(context, message, message_size);

  case MUMBLE_MESSAGE_PERMISSIONDENIED: // 12
    return mumble_private_process_permissiondenied(context, message, message_size);

  case MUMBLE_MESSAGE_ACL: // 13
    return mumble_private_process_acl(context, message, message_size);

  case MUMBLE_MESSAGE_QUERYUSERS: // 14
    return mumble_private_process_queryusers(context, message, message_size);

  case MUMBLE_MESSAGE_CRYPTSETUP: // 15
    return mumble_private_process_cryptsetup(context, message, message_size);

  case MUMBLE_MESSAGE_CONTEXTACTIONMODIFY: // 16
    return mumble_private_process_contextactionmodify(context, message, message_size);

  case MUMBLE_MESSAGE_CONTEXTACTION: // 17
    return mumble_private_process_contextaction(context, message, message_size);

  case MUMBLE_MESSAGE_USERLIST: // 18
    return mumble_private_process_userlist(context, message, message_size);

  case MUMBLE_MESSAGE_VOICETARGET: // 19
    return mumble_private_process_voicetarget(context, message, message_size);

  case MUMBLE_MESSAGE_PERMISSIONQUERY: // 20
    return mumble_private_process_permission_query(context, message, message_size);

  case MUMBLE_MESSAGE_CODECVERSION: // 21
    return mumble_private_process_codecversion(context, message, message_size);

  case MUMBLE_MESSAGE_USERSTATS: // 22
    return mumble_private_process_userstats(context, message, message_size);

  case MUMBLE_MESSAGE_REQUESTBLOB: // 23
    return mumble_private_process_requestblob(context, message, message_size);

  case MUMBLE_MESSAGE_SERVERCONFIG: // 24
    return mumble_private_process_serverconfig(context, message, message_size);

  case MUMBLE_MESSAGE_SUGGESTCONFIG: // 25
    return mumble_private_process_suggestconfig(context, message, message_size);

  default:
    printf("Unsupported mumble message type (%d)\n", message_type);
  }

  return -1;
}


// ********************* PUBLIC FUNCTIONS ***********************

struct mumble_struct* mumble_connect(void* talloc_context, struct mumble_config* config)
{
  if (config->size != sizeof(struct mumble_config))
  {
    printf("Config size is not correct. Library version is probably older or newer\n");
    return NULL;
  }

  struct mumble_struct* context = (struct mumble_struct*)talloc(talloc_context, struct mumble_struct);
  memset(context, 0, sizeof(struct mumble_struct));

  if (config->host == NULL)
  {
    printf("Host cannot be null\n");
    mumble_close(context);
    return NULL;
  }
  context->host = talloc_strdup(context, config->host);

  if (config->port == NULL)
  {
    config->port = "64738";
  }
  context->port = talloc_strdup(context, config->port);

  if (config->server_password != NULL)
  {
    context->server_password = talloc_strdup(context, config->server_password);
  }

  if (config->username == NULL)
  {
    printf("Need to specify a username to use\n");
    mumble_close(context);
    return NULL;
  }
  context->username = talloc_strdup(context, config->username);
  context->last_ping = time(NULL);

  context->state = MUMBLE_CLIENTSTATE_INPROGRESS;

  // Set callbacks
  context->version_callback             = config->version_callback;
  context->audio_callback               = config->audio_callback;
  context->serversync_callback          = config->serversync_callback;
  context->channelremove_callback       = config->channelremove_callback;
  context->channelstate_callback        = config->channelstate_callback;
  context->userremove_callback          = config->userremove_callback;
  context->userstate_callback           = config->userstate_callback;
  context->banlist_callback             = config->banlist_callback;
  context->textmessage_callback         = config->textmessage_callback;
  context->permissiondenied_callback    = config->permissiondenied_callback;
  context->acl_callback                 = config->acl_callback;
  context->queryusers_callback          = config->queryusers_callback;
  context->cryptsetup_callback          = config->cryptsetup_callback;
  context->contextactionmodify_callback = config->contextactionmodify_callback;
  context->contextaction_callback       = config->contextaction_callback;
  context->userlist_callback            = config->userlist_callback;
  context->voicetarget_callback         = config->voicetarget_callback;
  context->permissionquery_callback     = config->permissionquery_callback;
  context->codecversion_callback        = config->codecversion_callback;
  context->userstats_callback           = config->userstats_callback;
  context->requestblob_callback         = config->requestblob_callback;
  context->serverconfig_callback        = config->serverconfig_callback;
  context->suggestconfig_callback       = config->suggestconfig_callback;

  context->user_data = config->user_data;

  context->ssl_connection = ssl_connection_connect(context, context->host, context->port, config->ssl_verification_callback);


  if (context->ssl_connection == NULL)
  {
    printf("Could not connect to remote endpoint\n");
    mumble_close(context);
    return NULL;
  }

  if (mumble_private_send_version(context) != 0)
  {
    printf("Failed to send client version details\n");
    mumble_close(context);
    return NULL;
  }

  if (mumble_private_authenticate(context) != 0)
  {
    printf("Failed to authenticate\n");
    mumble_close(context);
    return NULL;
  }

  while (context->state == MUMBLE_CLIENTSTATE_INPROGRESS)
  {
    if (mumble_tick(context) < 0)
    {
      printf("Failed to fully establish connection\n");
      mumble_close(context);
      return NULL;
    }
  }

  return context;
}

int mumble_tick_full(struct mumble_struct* context, struct timeval* tv, fd_set* readfds, fd_set* writefds, fd_set* exceptfds, int max_fd)
{
  if (context->state == MUMBLE_CLIENTSTATE_FAILED)
  {
    return -1;
  }

  uint16_t type;
  uint8_t buffer[1024];

  int64_t time_till_ping = (int64_t)(context->last_ping + 20) - (int64_t)time(NULL);
  if (time_till_ping < 0)
  {
    time_till_ping = 0;
  }

  struct timeval tv_default;
  if (tv == NULL)
  {
    tv = &tv_default;
    tv_default.tv_sec = time_till_ping;
    tv_default.tv_usec = 0;
  } else
  {
    if (tv->tv_sec > time_till_ping)
    {
      tv->tv_sec = time_till_ping;
    }
  }

  int ret = mumble_private_read(context, &type, buffer, sizeof(buffer), tv, readfds, writefds, exceptfds, max_fd);
  if (ret > 0)
  {
    mumble_private_process_message(context, type, buffer, ret);

    // Check to see if we're due to ping
    if ((context->last_ping + 20) <= time(NULL))
    {
      mumble_send_server_ping(context);
    }

    return 1;
  } else if (ret == 0)
  {
    // Should send ping if timeout expires? (yes)
    mumble_send_server_ping(context);
  } else if (ret < 0)
  {
    printf("Error getting message: %d\n", ret);
    context->state = MUMBLE_CLIENTSTATE_FAILED;
  }

  return ret;
}

int mumble_tick(struct mumble_struct* context)
{
  return mumble_tick_full(context, NULL, NULL, NULL, NULL, 0);
}

// Buffer must be at least 8 bytes
uint32_t mumble_make_varint(int64_t val, uint8_t* buffer)
{
  if (val < 0)
  {
    printf("Currently negative varints aren't supported\n");
    exit(0);
  }

  if (val < 0x80)
  {
    buffer[0] = val;
    return 1;
  }

  if (val < 0x4000)
  {
    buffer[0] = 0x80 | (val >> 8);
    buffer[1] = val & 0xFF;
    return 2;
  }

  if (val < 0x200000)
  {
    buffer[0] = 0xC0 | (val >> 16);
    buffer[1] = (val >> 8) & 0xFF;
    buffer[2] = val & 0xFF;
    return 3;
  }

  if (val < 0x10000000)
  {
    buffer[0] = 0xE0 | (val >> 24);
    buffer[1] = (val >> 16) & 0xFF;
    buffer[2] = (val >> 8) & 0xFF;
    buffer[3] = val & 0xFF;
    return 4;
  }

  printf("Currently unsupported varint size %lld\n", (long long int)val);
  exit(0);
}

uint32_t mumble_parse_variant(int64_t *val, uint8_t *buffer)
{
  int64_t v = buffer[0];
  if ((v & 0x80) == 0x00) {
    *val = (v & 0x7F);
    return 1;
  } else if ((v & 0xC0) == 0x80) {
    *val = (v & 0x3F) << 8 | buffer[1];
    return 2;
  } else if ((v & 0xF0) == 0xF0) {
    switch (v & 0xFC) {
      case 0xF0:
        *val = buffer[1] << 24 | buffer[2] << 16 | buffer[3] << 8 | buffer[4];
            return 5;
      case 0xF4:
        printf("Currently unsupported 8-byte varint size\n");
            exit(0);
      case 0xF8:
      case 0xFC:
        printf("Currently negative varints aren't supported\n");
            exit(0);
            break;
      default:

        break;
    }
  } else if ((v & 0xF0) == 0xE0) {
    *val = (v & 0x0F) << 24 | buffer[1] << 16 | buffer[2] << 8 | buffer[3];
    return 4;
  } else if ((v & 0xE0) == 0xC0) {
    *val = (v & 0x1F) << 16 | buffer[1] << 8 | buffer[2];
    return 3;
  }
  printf("Invalid varint\n");
  exit(0);
}

int mumble_send_audio_data(struct mumble_struct* context, uint32_t sequence_num, uint8_t* audio_data, uint32_t audio_data_size)
{
  if (audio_data_size > 127)
  {
    printf("Warning truncating data!\n");
    audio_data_size = 127;
  }

  uint8_t varint[8];
  uint32_t varint_size = mumble_make_varint(sequence_num, varint);
  uint32_t buffer_size = 1 + varint_size + 1 + audio_data_size;

  uint8_t* buffer = (uint8_t*)talloc_size(context, buffer_size);
  buffer[0] = 0x80;
  memcpy(buffer + 1, varint, varint_size);
  buffer[1 + varint_size] = audio_data_size;
  memcpy(buffer + 1 + varint_size + 1, audio_data, audio_data_size);

  int ret = mumble_private_write(context, MUMBLE_MESSAGE_UDPTUNNEL, buffer, buffer_size);
 
  talloc_free(buffer);

  if (ret != buffer_size)
  {
    printf("Failed to send entire audio buffer\n");
    return -1;
  }

  return 0;
}

int mumble_send_server_ping(struct mumble_struct* context)
{
  MumbleProto__Ping ping = MUMBLE_PROTO__PING__INIT;
  
  // We've sent a ping. Let's wait a second before sending another
  // If we receive a pong we'll update this to the current time
  context->last_ping++;

  ping.has_timestamp = 1;
  ping.timestamp = time(NULL);

  uint32_t message_size = mumble_proto__ping__get_packed_size(&ping);

  uint8_t* message = (uint8_t*)talloc_size(context, message_size);
  mumble_proto__ping__pack(&ping, message);

  int ret = mumble_private_write(context, MUMBLE_MESSAGE_PING, message, message_size);
  talloc_free(message);

  if (ret != message_size)
  {
    printf("Failed to send ping message\n");
    return -1;
  }

  return 0;
}

void mumble_close(struct mumble_struct* context)
{
  if (context->ssl_connection != NULL)
  {
    ssl_connection_disconnect(context->ssl_connection);
    context->ssl_connection = NULL;
  }
  talloc_free(context);
}

