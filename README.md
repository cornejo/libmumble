# libmumble
Simple single-threaded implementation of the mumble protocol written in C.

It is single threaded and uses no locking - therefore is not thread safe.

If you wish to use this in an environment where multiple threads may interact with it you will have to implement your own synchronisation.

# Compilation Requirements

* Talloc
* protobuf-c-compiler
* scons

* OpenSSL or GnuTLS (preferred)

# Compiling
Just type scons

If you'd like to change to a different TLS library (like openssl) type

scons --tls=openssl

# TODO
- Figure out how to pass a client cert to ssl_connection
- Support OpenSSL v1.0.2
- Figure out how to inform the TLS library of which certificates are considered valid (so that the library can do the complete authentication steps, including hostname validation)
- Complete mumble spec support (specifically the client->server requests)
- Add support for all server messages (ACL/UserStats)
- Create simple test example


